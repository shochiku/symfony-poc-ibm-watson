<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BrandNewController extends AbstractController
{
    /**
     * @Route("/", name="brand_new")
     */
    public function index()
    {

        $client = new \GuzzleHttp\Client();

        // Fetch the text in JSON format
        $sampleText = $client->request(
            'GET',
            'https://watson-developer-cloud.github.io/doc-tutorial-downloads/tone-analyzer/tone.json'
        )->getBody();

        // Send the text to Tone Analyzer
        $analyzedText = $client->request(
            'GET', 
            $_ENV['TONE_ANALYZER_URL'] . '?version=2017-09-21&text=' . urlencode($sampleText),
            ['auth' => ['apikey', $_ENV['TONE_ANALYZER_IAM_APIKEY']]]
        )->getBody();

        return $this->render('brand_new/index.html.twig', [
            'sampleText' => $sampleText,
            'analyzedText' => $analyzedText
        ]);
    }
}
